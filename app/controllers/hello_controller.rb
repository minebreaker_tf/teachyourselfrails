class HelloController < ApplicationController

  def hello
    @name = params[:name].blank? ? 'world' : params[:name]

    render 'hello/hello'
  end

end
